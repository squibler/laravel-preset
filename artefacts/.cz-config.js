'use strict';

module.exports = {

    types: [
        {value: 'WIP',      name: 'WIP:      Work in progress'},
        {value: 'Feature',  name: 'Feature:  A new feature'},
        {value: 'Fix',      name: 'Fix:      A bug fix'},
        {value: 'Refactor', name: 'Refactor: A code change that neither fixes a bug nor adds a feature'},
        {value: 'Docs',     name: 'Docs:     Documentation only changes'},
        {value: 'Test',     name: 'Test:     Adding missing tests'},
        {value: 'Chore',    name: 'Chore:    Changes to the build process or auxiliary tools\n            and libraries such as documentation generation'},
    ],

    scopes: [
        {name: 'backend'},
        {name: 'frontend'},
        {name: 'console'},
    ],

    // it needs to match the value for field type. Eg.: 'fix'
    scopeOverrides: {
        WIP: null,
        Docs: null,
    },

    // override the messages, defaults are as follows
    messages: {
        type: 'Select the type of change that you\'re committing:',
        scope: '\nDenote the SCOPE of this change (optional):',
        // used if allowCustomScopes is true
        customScope: 'Denote the SCOPE of this change:',
        subject: 'Write a SHORT, IMPERATIVE tense description of the change:\n',
        body: 'Provide a LONGER description of the change (optional). Use "|" to break new line:\n',
        breaking: 'List any BREAKING CHANGES (optional):\n',
        footer: 'List any ISSUES CLOSED by this change (optional). E.g.: #31, #34:\n',
        confirmCommit: 'Are you sure you want to proceed with the commit above?'
    },

    allowCustomScopes: true,
    allowBreakingChanges: ['Feature', 'Fix', 'WIP']
};