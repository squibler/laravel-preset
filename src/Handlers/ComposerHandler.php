<?php

namespace Squibler\Preset\Handlers;

use Squibler\Preset\Support\Traits\FileTrait;

class ComposerHandler implements HandlerInterface
{
    use FileTrait;

    public static function Handle($options, $command)
    {
        $decoded = [];
        $dependencies = [];
        $file = base_path('composer.json');
        if (file_exists($file)) {
            $decoded = json_decode(file_get_contents($file), true);
        }

        if (empty($options)) {
            return;
        }

        foreach ($options as $key => $data) {
            if (empty($data)) {
                continue;
            }

            if (in_array($key, ['require', 'requireDev'])) {
                $dependencies[$key] = $data;
                continue;
            }

            $command->warn('- Updating ' . $key);
            if (in_array($key, ['scripts', 'configs'])) { // Merge
                $original = isset($decoded[ $key ]) ? $decoded[ $key ] : [];
                $decoded[ $key ] = array_merge($original, $data);
            } else {
                $decoded[ $key ] = $data;
            }
        }

        $command->warn('- Writing to file');
        file_put_contents(
            $file,
            json_encode($decoded, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT).PHP_EOL
        );

        $command->warn('- Installing Dependencies');
        static::installDependencies($dependencies);
    }

    private static function installDependencies(array $dependencies)
    {
        $basepath = base_path();
        shell_exec("cd ${basepath};");

        foreach ($dependencies as $key => $data) {
            if (empty($data)) {
                continue;
            }
            $requirements = join(' ', $data);
            $flags = ($key === 'require') ? '' : '--dev';
            shell_exec("composer require ${flags} ${requirements}");
        }
    }
}
