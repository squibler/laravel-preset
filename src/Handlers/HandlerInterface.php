<?php

namespace Squibler\Preset\Handlers;

interface HandlerInterface
{
    public static function Handle($options, $command);
}
