<?php

namespace Squibler\Preset\Handlers;

use Squibler\Preset\Support\Traits\FileTrait;

class IgnoreHandler implements HandlerInterface
{
    use FileTrait;

    public static function Handle($options, $command)
    {
        $gitignore = base_path('.gitignore');
        self::append2File('/presets.yml', $gitignore);

        $rules = is_array($options) ? $options : [];
        foreach ($rules as $rule) {
            self::append2File($rule, $gitignore);
        }
    }
}
