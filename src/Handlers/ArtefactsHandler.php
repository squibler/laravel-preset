<?php

namespace Squibler\Preset\Handlers;

class ArtefactsHandler implements HandlerInterface
{
    public static function Handle($options, $command)
    {
        if (!is_array($options)) {
            return;
        }

        $basepath = base_path();
        $artifactpath = realpath(__DIR__ . '/../../artefacts');

        shell_exec("cd ${basepath};");

        foreach ($options as $artifact => $destination) {
            $command->warn('- Attempting to copy artifact to defined location');

            $from = $artifactpath . DIRECTORY_SEPARATOR . $artifact;
            $to = base_path($destination);
            $path = dirname($to);

            if (!file_exists($from)) {
                $command->error('  - Artifact ' . $from . ' does not exist in ' . $artifactpath);
            }

            $origUmask = umask();
            umask(0000);
            if (! ( is_dir($path) || file_exists($path) || mkdir($path, 0777, true) )) {
                $command->error('  - Error while creating directory ' . $path . ' for artefact');
                umask($origUmask);
                continue;
            }
            if ( !copy($from, $to)) {
                $command->error( '  - Failed to copy artefact to ' . $to );
                umask($origUmask);
                continue;
            }
            chmod($to, 0664);
            chmod($path, 0755);
            umask($origUmask);
        }
    }
}
