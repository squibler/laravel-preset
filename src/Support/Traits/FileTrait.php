<?php

namespace Squibler\Preset\Support\Traits;

use File;

trait FileTrait
{
    protected static function replaceStringInFile($replace, $with, $infile)
    {
        if (File::isFile($infile)) {
            $content = file_get_contents($infile);
            $content = str_replace($replace, $with, $content);
            file_put_contents($infile, $content);
        }
    }


    protected static function append2File($content, $file)
    {
        if (File::isFile($file)) {
            $text = file_get_contents($file);
            if (strpos($text, $content) !== false) {
                return;
            }
            $content .= "\n";
        }

        file_put_contents($file, $content, FILE_APPEND);
    }
}
