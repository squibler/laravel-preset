<?php

namespace Squibler\Preset\Providers;

use Illuminate\Foundation\Console\PresetCommand;
use Squibler\Preset\Console\Commands\SquibPresetMopCommand;
use Squibler\Preset\Console\Presets\Squib;
use Squibler\Preset\Console\Commands\LogicMakeCommand;

class PresetsServiceProvider extends ServiceProvider
{
    const PACKAGE_CONFIG_PATH = __DIR__.'/../../config/';

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app['env'] === 'local') {
            $this->commands([
                SquibPresetMopCommand::class
            ]);

            $this->publishes([
                self::PACKAGE_CONFIG_PATH . 'preset.json' => base_path('preset.json'),
            ]);

            PresetCommand::macro('squib', function ($command) {
                Squib::install($command);
            });
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
