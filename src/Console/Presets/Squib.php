<?php

namespace Squibler\Preset\Console\Presets;

use Illuminate\Support\Arr;
use Illuminate\Foundation\Console\PresetCommand;
use Illuminate\Foundation\Console\Presets\Preset as LaravelPreset;
use File;
use Squibler\Preset\Support\Traits\FileTrait;
use Squibler\Preset\Handlers\HandlerInterface;

class Squib extends LaravelPreset
{
    use FileTrait;

    private static $config;
    private static $cleansers = [];


    protected static function config($key = null)
    {
        if (! static::$config) {
            $userConfig = base_path('preset.json');
            $file = ( ! File::isFile($userConfig) )
                  ? base_path('vendor/squibler/laravel-preset/config/preset.json')
                  : $userConfig;

            static::$config = collect(json_decode(file_get_contents($file), true));
        }

        if ($key) {
            return static::$config->has($key) ? static::$config->get($key) : [];
        } else {
            return static::$config;
        }
    }


    public static function install(PresetCommand $command)
    {
        $command->line('');

        // 1. Clean up
        $command->warn('Cleaning up');
        static::cleanSassDirectory($command);
        static::removeComponents($command);
        static::removeNodeModules();
        static::updateModels($command);
        $command->info("Cleaned up\n");

        $command->warn('Applying presets from config');
        $config = static::config();
        foreach ($config as $key => $options) {
            $command->line('');
            $name = title_case($key);
            $command->warn('- Handling ' . $name);

            if (empty($options)) {
                $command->warn('- No options for ' . $name);
                continue;
            }

            $handler = '\\Squibler\\Preset\\Handlers\\' . studly_case(strtolower($key) . '-handler');
            if (! class_exists($handler)) {
                $command->error('- No handler found for ' . $key . ' config');
                continue;
            }

            // if ( ! ( $handler instanceof HandlerInterface ) ) {
            //     $command->error( '- Handler must be an instance of HandlerInterface' );
            //     continue;
            // }

            call_user_func($handler . '::Handle', $options, $command);
            $command->warn('- Completed with ' . $name);
        }

        // static::updateIgnore( $command );
        // static::updateComposer( $command );
        // static::handlePackages( $command );

        $command->info("Preset Applied\n");
        $command->error("------------------[ COMMIT PRESET SCRIPT CREATED BY SQUIBLER 🍺 ]------------------\n");
    }


    public static function cleanSassDirectory($command)
    {
        $command->warn('Removing assets/sass');

        File::deleteDirectory(resource_path('assets/sass'));

        $command->info("Done...\n");
    }


    protected static function removeComponents($command)
    {
        $command->warn('Removing assets/js');
        File::deleteDirectory(resource_path('assets/js'));
        $command->info("Done...\n");
    }


    public static function updateModels($command)
    {
        $command->warn('Add app/Models directory and update model references');

        $modelPath = app_path('Models');
        $userModel = $modelPath . '/User.php';

        if (! File::isDirectory($modelPath)) {
            $command->info("- Create app/Models directory");
            File::makeDirectory($modelPath);
        }

        $command->info("- Move app/User.php to app/Models/User.php");

        if (File::isFile(app_path('User.php'))) {
            File::move(app_path('User.php'), $userModel);
            $command->info("- Update User.php namespaces");
            self::replaceStringInFile('namespace App;', 'namespace App\Models;', $userModel);
        }

        $command->info("- Update references to app\User");
        $files = [
            'app/Http/Controllers/Auth/RegisterController.php',
            'config/auth.php',
            'config/services.php',
            'database/factories/UserFactory.php',
        ];
        foreach ($files as $file) {
            $command->warn("  - $file");
            self::replaceStringInFile('App\\User', 'App\Models\User', base_path($file));
        }
        $command->info("- Moved Models into Models Directory");
        $command->info("Done...\n");
    }
}
