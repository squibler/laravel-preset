<?php

namespace Squibler\Preset\Console\Commands;

use Illuminate\Console\Command;
use File;

class SquibPresetMopCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'squib:preset:mop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mop up files that are only needed for the initial Apply preset';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = base_path('presets.json');
        if (File::isFile($file)) {
            File::delete($file);
        }

        shell_exec('composer remove --dev squibler/laravel-preset');
    }
}
