# Laravel Preset

A configurable preset for Laravel with sensible defaults which:

- Applies default packages and settings to both `packages.json` and `composer.json`
- Updates .gitignore with default preferences
- Writes default artefacts into your directory structure
- Moves models into an app/Models directory
- Adds commitizen for standardised commit messages

## Usage

## 1. Install to dev using composer

```bash
composer require --dev squibler/laravel-preset
```

## 2. Update the preset settings (optional)

Now make changes (if any) to the initial packages you want installed using the preset config.

Publish the preset.json to your project root to update the preset settings.

After the initial set up, it's expected you will never use the preset again which is why it gets ignored.

```bash
php artisan vendor:publish --provider='Squibler\Preset\Providers\PresetsServiceProvider'
```

The default preset configuration is designed with an API only applictaion in mind:

```json
{
    "composer": {
        "scripts": {
            "test": "phpunit"
        },
        "require": [
            "bensampo/laravel-enum",
            "laravel/passport",
            "rexlabs/laravel-smokescreen"
        ],
        "requireDev": [
            "phpunit/phpunit",
            "squibler/laravel-artisan",
            "squizlabs/php_codesniffer"
        ]
    },
    "packages": {
        "scripts": {},
        "dependencies": [],
        "devDependencies": [
            "cz-customizable"
        ],
        "configs": {
            "commitizen": {
                "path": "node_modules/cz-customizable"
            }
        }
    },
    "ignore": [
        "_dev", ".vscode"
    ],
    "artefacts": {
        ".cz-config.js": ".cz-config.js"
    }
}
```

## 3. Apply your preset

You're now ready to apply your own defaults to Laravel, you can apply the preset using:

```bash
php artisan preset squib
```

## 4. Clean up

Applying a preset is only something you will do once on a project, so there is a helper command to clean up if you choose to. It's not required as sensible default ignores are added to the .gitignore file. But if you use something like EB deploy then this is something you probably will want to do.

```bash
php artisan squib:preset:mop
```

---

## What does this preset do

I wanted to make a preset that could be configured easily on a per project basis but also had some sensible defaults out of the box

So by default it's set up with a API configuration as most of my Laravel projects are API motivated

- Removes unneeded Javascript Libraries
- Updates the .gitignore
- Removes the assests/sass Directory
- Removes the assets/js
- Updates and installs composer packages if any
  - laravel/passport
  - rexlabs/laravel-smokescreen
- Updates and installs npm packages if any or...
- Removes node_modules (if no packages)
- Moves User model to app/Models dir

---

## What's next

My plan is to make this even more configuarable - for example, the javascript libraries are all removed similar to the default Laravel `php artisan preset none`
command but I would prefer if they could be chosen on a per project basis.

I am also contemplating creating a presets repo, where people can keep their own
set of defaults and then possibly something like:

```bash
php artisan squib:preset:keep namespace/name # To store the preset.json options
php artisan squib:preset:use namespace/name # to fetch the preset.json file
```
